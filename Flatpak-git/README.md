# This Flatpak files (*.yml) will build from the latest git commits. 

Go to [BuildInfo - Flatpak](https://gitlab.com/cubocore/wiki/-/blob/master/BuildInfo.md#build-a-flatpak-package) for more info.

You can download C-Suite flatpak from [flathub](https://flathub.org/apps/search/CSuite).


# Example Build Command for CoreUniverse

```sh
flatpak-builder build-dir --force-clean org.cubocore.CoreUniverse.yml
flatpak-builder --user --install --force-clean build-dir org.cubocore.CoreUniverse.yml  
flatpak run org.cubocore.CoreUniverse
```
